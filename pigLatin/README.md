Este ejercicio es complicado y se eliminó de nuestras recomendaciones porque en su mayoría utiliza expresiones regulares para la solución, y esas no se enseñan realmente en este punto de nuestro plan de estudios.

Lo dejamos aquí para la posteridad, o como un buen desafío para cualquiera que quiera intentarlo.

Pig Latin es un lenguaje infantil que pretende ser confuso cuando se habla rápidamente. Tu tarea para este ejercicio es crear una solución que tome las palabras dadas
y las convierta a pig latin. Consulta la siguiente página de Wikipedia para obtener detalles sobre las reglas del Pig Latin:

https://en.wikipedia.org/wiki/Pig_Latin

La sección de reglas proporcionará las reglas y los ejemplos necesarios para completar este ejercicio.
