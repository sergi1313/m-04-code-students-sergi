# Ejercicio 06 - Años bisiestos

Crea una función que determine si un año dado es bisiesto. Los años bisiestos se determinan mediante las siguientes reglas:

> Los años bisiestos son aquellos divisibles por cuatro (como 1984 y 2004). Sin embargo, los años divisibles por 100 no son bisiestos (como 1800 y 1900) a menos que también sean divisibles por 400 (como 1600 y 2000, que de hecho fueron años bisiestos). (Sí, todo es bastante confuso)

-- [Aprende a Programar](https://pine.fm/LearnToProgram/chap_06.html)

```javascript
leapYears(2000) // is a leap year: returns true
leapYears(1985) // is not a leap year: returns false
```

## Pistas
- Utiliza una declaración `if` y `&&` para asegurarte de que se cumplan todas las condiciones correctamente
