# Ejercicio 07 - tempConversion

Escribe dos funciones que conviertan temperaturas de Fahrenheit a Celsius y viceversa:

```
ftoc(32) // de Fahrenheit a Celsius, debería devolver 0

ctof(0) // de Celsius a Fahrenheit, debería devolver 32
```


Dado que somos humanos, queremos que la temperatura resultante esté redondeada a un decimal: es decir, `ftoc(100)` debería devolver `37.8` y no `37.77777777777778`.

Este ejercicio te pide que crees más de una función, por lo que la sección `module.exports` del archivo de especificaciones luce un poco diferente esta vez. No te preocupes, simplemente estamos empaquetando ambas funciones en un solo objeto para ser exportadas.

## Pistas
- Puedes encontrar las fórmulas relevantes en [Wikipedia](https://en.wikipedia.org/wiki/Conversion_of_units_of_temperature).

- Intenta encontrar por ti mismo en Internet cómo redondear un número a un decimal en JavaScript. Si tienes dificultades, echa un vistazo [aquí](https://stackoverflow.com/q/7342957/5433628).
